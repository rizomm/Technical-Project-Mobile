package com.rizomm.cargodenuit.providers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rizomm.cargodenuit.common.helpers.Logger;
import com.rizomm.cargodenuit.common.models.Booking;
import com.rizomm.cargodenuit.database.ORMLiteDatabaseHelper;

import java.sql.SQLException;
import java.util.List;

public class BookingProvider {

    public static void getBookings(Context context) {
        try {
            List<Booking> toInsert = null; //Todo : link with Web Service
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Booking, String> daoBookings = helper.getBookingDao();

            for (Booking booking: toInsert) {
                daoBookings.create(booking);
            }

            helper.close();
        } catch (SQLException e) {
            Logger.Error("Error when inserting bookings to DB", e);
        }
    }

    public static List<Booking> getAllBookings(Context context) {
        List<Booking> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Booking, String> daoBookings = helper.getBookingDao();

            result = daoBookings.queryForAll();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting bookings to DB", e);
        }

        return result;
    }

    public static Booking getBookingById(Context context, int id) {
        Booking result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Booking, String> daoBookings = helper.getBookingDao();

            QueryBuilder<Booking, String> qbUsers = daoBookings.queryBuilder();
            qbUsers.where().eq(Booking.FIELD_BOOKING_ID, id);

            result = qbUsers.queryForFirst();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting booking by id to DB", e);
        }

        return result;
    }
}
