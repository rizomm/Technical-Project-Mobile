package com.rizomm.cargodenuit.controllers.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rizomm.cargodenuit.R;
import com.rizomm.cargodenuit.controllers.activity.MainActivity;
import com.rizomm.cargodenuit.global.ApplicationSharedPreferences;
import com.rizomm.cargodenuit.providers.UserProvider;

public class SplashScreenFragment extends Fragment {

    // Members
    // --------------------------------------------------------------------------------------------
    private void initializeViews(View v) {

    }

    // Life cycle
    // --------------------------------------------------------------------------------------------
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_splash, container, false);

        initializeViews(v);

        if (ApplicationSharedPreferences.getInstance(getActivity().getApplicationContext()).isFirstLaunch()) {
            new ParseTask().execute();
            ApplicationSharedPreferences.getInstance(getActivity().getApplicationContext()).setFirstLaunch(false);
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showMainActivity();
                }
            }, 1000);
        }

        return v;
    }

    // Views navigation
    // --------------------------------------------------------------------------------------------
    private void showMainActivity() {
        startActivity(new Intent(getActivity().getApplicationContext(), MainActivity.class));
        getActivity().finish();
    }

    // AsyncTask
    // --------------------------------------------------------------------------------------------
    private class ParseTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            //TODO : import Users and others Data

            //BaseProvider.parseAirports(getActivity().getApplicationContext());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            showMainActivity();
        }

    }
}
