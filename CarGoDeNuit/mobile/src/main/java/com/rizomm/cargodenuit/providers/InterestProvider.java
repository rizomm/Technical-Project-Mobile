package com.rizomm.cargodenuit.providers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rizomm.cargodenuit.common.helpers.Logger;
import com.rizomm.cargodenuit.common.models.Interest;
import com.rizomm.cargodenuit.database.ORMLiteDatabaseHelper;

import java.sql.SQLException;
import java.util.List;

public class InterestProvider {

    public static void getInterests(Context context) {
        try {
            List<Interest> toInsert = null; //Todo : link with Web Service
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Interest, String> daoInterests = helper.getInterestDao();

            for (Interest inte: toInsert) {
                daoInterests.create(inte);
            }

            helper.close();
        } catch (SQLException e) {
            Logger.Error("Error when inserting interests to DB", e);
        }
    }

    public static List<Interest> getAllInterests(Context context) {
        List<Interest> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Interest, String> daoInterests = helper.getInterestDao();

            result = daoInterests.queryForAll();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting interests to DB", e);
        }

        return result;
    }

    public static Interest getInterestById(Context context, int id) {
        Interest result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Interest, String> daoInterests = helper.getInterestDao();

            QueryBuilder<Interest, String> qbUsers = daoInterests.queryBuilder();
            qbUsers.where().eq(Interest.FIELD_INTEREST_ID, id);

            result = qbUsers.queryForFirst();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting interest by model to DB", e);
        }

        return result;
    }
}
