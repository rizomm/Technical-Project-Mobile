package com.rizomm.cargodenuit.helpers;

import com.rizomm.cargodenuit.common.helpers.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateHelper {
	
	public static Date convertDate(Date date) {
		if (date != null) {
            Date result = null;

            DateFormat cetFormat = new SimpleDateFormat();
            DateFormat gmtFormat = new SimpleDateFormat();
            TimeZone gmtTime = TimeZone.getTimeZone("GMT");
            TimeZone cetTime = TimeZone.getTimeZone("CET");
            cetFormat.setTimeZone(gmtTime);
            gmtFormat.setTimeZone(cetTime);

            try {
                result = cetFormat.parse(gmtFormat.format(date));
            } catch (Exception e) {
                Logger.Error("Error converting date");
            }

            return result;
        } else {
            return null;
        }
	}
	
	public static Date convertDateToGmt(Date date) {
		Date result = null;
		
		DateFormat cetFormat = new SimpleDateFormat();
		DateFormat gmtFormat = new SimpleDateFormat();
		TimeZone gmtTime = TimeZone.getTimeZone("GMT");
		TimeZone cetTime = TimeZone.getTimeZone("CET");
		cetFormat.setTimeZone(gmtTime);
		gmtFormat.setTimeZone(cetTime);
		
		try {
			result = gmtFormat.parse(cetFormat.format(date));
		} catch (Exception e) {
			Logger.Error("Error converting date");
		}
		
		return result;
	}
}
