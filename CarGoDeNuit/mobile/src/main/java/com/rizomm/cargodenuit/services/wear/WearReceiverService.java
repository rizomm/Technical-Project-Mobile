package com.rizomm.cargodenuit.services.wear;

import com.rizomm.cargodenuit.common.global.Constants;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

public class WearReceiverService extends WearableListenerService {

    // Constants
    // --------------------------------------------------------------------------------------------

    // Members
    // --------------------------------------------------------------------------------------------

    // Implementation
    // --------------------------------------------------------------------------------------------
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
        if (messageEvent.getPath().equals(Constants.PREVIOUS_STEP_MESSAGE_PATH)) {

        } else if (messageEvent.getPath().equals(Constants.NEXT_STEP_MESSAGE_PATH)) {

        }
    }

}