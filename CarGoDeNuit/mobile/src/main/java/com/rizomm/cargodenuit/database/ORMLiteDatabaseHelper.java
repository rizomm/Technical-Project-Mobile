package com.rizomm.cargodenuit.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.rizomm.cargodenuit.common.helpers.Logger;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.rizomm.cargodenuit.common.models.Booking;
import com.rizomm.cargodenuit.common.models.Brand;
import com.rizomm.cargodenuit.common.models.Car;
import com.rizomm.cargodenuit.common.models.Fuel;
import com.rizomm.cargodenuit.common.models.Interest;
import com.rizomm.cargodenuit.common.models.KmCost;
import com.rizomm.cargodenuit.common.models.Model;
import com.rizomm.cargodenuit.common.models.Music;
import com.rizomm.cargodenuit.common.models.Trip;
import com.rizomm.cargodenuit.common.models.User;
import com.rizomm.cargodenuit.common.models.UserType;

import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class ORMLiteDatabaseHelper extends OrmLiteSqliteOpenHelper {

    // Constants
    // --------------------------------------------------------------------------------------------
	private static final String DATABASE_NAME = "cargo_de_nuit_database";
	private static final int DATABASE_VERSION = 1;

	private static final AtomicInteger usageCounter = new AtomicInteger(0);

    // Members
    // --------------------------------------------------------------------------------------------
	private Dao<User, String> userDAO           = null;
    private Dao<Car, String> carDAO             = null;
    private Dao<Brand, String> brandDAO         = null;
    private Dao<Booking, String> bookingDAO     = null;
    private Dao<Fuel, String> fuelDAO           = null;
    private Dao<Interest, String> interestDAO   = null;
    private Dao<KmCost, String> kmCostDAO       = null;
    private Dao<Model, String> modelDAO         = null;
    private Dao<Music, String> musicDAO         = null;
    private Dao<Trip, String> tripDAO           = null;
    private Dao<UserType, String> userTypeDAO   = null;

	private static ORMLiteDatabaseHelper helper = null;

    // Constructor
    // --------------------------------------------------------------------------------------------
	private ORMLiteDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * Get the helper, possibly constructing it if necessary. For each call to this method, there should be 1 and only 1
	 * call to {@link #close()}.
	 */
	public static synchronized ORMLiteDatabaseHelper getHelper(Context context) {
		if (helper == null) {
			helper = new ORMLiteDatabaseHelper(context);
		}
		usageCounter.incrementAndGet();
		return helper;
	}

	/**
	 * This is called when the database is first created. Usually you should call createTable statements here to create
	 * the tables that will store your data.
	 */
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
            Logger.Debug("onCreate");
			TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Car.class);
            TableUtils.createTable(connectionSource, Brand.class);
            TableUtils.createTable(connectionSource, Booking.class);
            TableUtils.createTable(connectionSource, Fuel.class);
            TableUtils.createTable(connectionSource, Interest.class);
            TableUtils.createTable(connectionSource, KmCost.class);
            TableUtils.createTable(connectionSource, Model.class);
            TableUtils.createTable(connectionSource, Music.class);
            TableUtils.createTable(connectionSource, Trip.class);
            TableUtils.createTable(connectionSource, UserType.class);
		} catch (SQLException e) {
			Logger.Error("Can't create database", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
	 * the various data to match the new version number.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		// Do Nothing
	}
	
	public void clearAllTables() {
		ConnectionSource connectionSource = getConnectionSource();
		
		try {
            Logger.Debug("Clear Table");
			TableUtils.clearTable(connectionSource, User.class);
            TableUtils.clearTable(connectionSource, Car.class);
            TableUtils.clearTable(connectionSource, Brand.class);
            TableUtils.clearTable(connectionSource, Booking.class);
            TableUtils.clearTable(connectionSource, Fuel.class);
            TableUtils.clearTable(connectionSource, Interest.class);
            TableUtils.clearTable(connectionSource, KmCost.class);
            TableUtils.clearTable(connectionSource, Model.class);
            TableUtils.clearTable(connectionSource, Music.class);
            TableUtils.clearTable(connectionSource, Trip.class);
            TableUtils.clearTable(connectionSource, UserType.class);
		} catch (SQLException e) {
			Logger.Error("Can't clear all tables", e);
		}
	}
	
	/**
	 * Returns the Database Access Object (DAO) for our SimpleData class. It will create it or just give the cached
	 * value.
	 */
	public Dao<User, String> getUserDao() throws SQLException {
		if (userDAO == null) {
			userDAO = getDao(User.class);
		}
		return userDAO;
	}

    public Dao<Car,String> getCarDao() throws SQLException {
        if (carDAO == null) {
            carDAO = getDao(Car.class);
        }
        return carDAO;
    }

    public Dao<Brand,String> getBrandDao() throws SQLException {
        if (brandDAO == null) {
            brandDAO = getDao(Brand.class);
        }
        return brandDAO;
    }

    public Dao<Booking,String> getBookingDao() throws SQLException {
        if (bookingDAO == null) {
            bookingDAO = getDao(Booking.class);
        }
        return bookingDAO;
    }

    public Dao<Fuel,String> getFuelDao() throws SQLException {
        if (fuelDAO == null) {
            fuelDAO = getDao(Fuel.class);
        }
        return fuelDAO;
    }

    public Dao<Interest,String> getInterestDao() throws SQLException {
        if (interestDAO == null) {
            interestDAO = getDao(Interest.class);
        }
        return interestDAO;
    }

    public Dao<KmCost,String> getKmCostDao() throws SQLException {
        if (kmCostDAO == null) {
            kmCostDAO = getDao(KmCost.class);
        }
        return kmCostDAO;
    }

    public Dao<Model,String> getModelDao() throws SQLException {
        if (modelDAO == null) {
            modelDAO = getDao(Model.class);
        }
        return modelDAO;
    }

    public Dao<Music,String> getMusicDao() throws SQLException {
        if (musicDAO == null) {
            musicDAO = getDao(Music.class);
        }
        return musicDAO;
    }

    public Dao<Trip,String> getTripDao() throws SQLException {
        if (tripDAO == null) {
            tripDAO = getDao(Trip.class);
        }
        return tripDAO;
    }

    public Dao<UserType,String> getUserTypeDao() throws SQLException {
        if (userTypeDAO == null) {
            userTypeDAO = getDao(UserType.class);
        }
        return userTypeDAO;
    }

	/**
	 * Close the database connections and clear any cached DAOs. For each call to {@link #getHelper(android.content.Context)}, there
	 * should be 1 and only 1 call to this method. If there were 3 calls to {@link #getHelper(android.content.Context)} then on the 3rd
	 * call to this method, the helper and the underlying database connections will be closed.
	 */
	@Override
	public void close() {
		if (usageCounter.decrementAndGet() == 0) {
			super.close();
			userDAO = null;
            carDAO = null;
            brandDAO = null;
            bookingDAO = null;
            fuelDAO = null;
            interestDAO = null;
            kmCostDAO = null;
            modelDAO = null;
            musicDAO = null;
            tripDAO = null;
            userTypeDAO = null;
			
			helper = null;
		}
	}
}
