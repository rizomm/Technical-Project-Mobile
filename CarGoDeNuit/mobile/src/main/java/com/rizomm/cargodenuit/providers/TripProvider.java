package com.rizomm.cargodenuit.providers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rizomm.cargodenuit.common.helpers.Logger;
import com.rizomm.cargodenuit.common.models.Trip;
import com.rizomm.cargodenuit.database.ORMLiteDatabaseHelper;

import java.sql.SQLException;
import java.util.List;

public class TripProvider {

    public static void getTrips(Context context) {
        try {
            List<Trip> toInsert = null; //Todo : link with Web Service
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Trip, String> daoFuels = helper.getTripDao();

            for (Trip trip: toInsert) {
                daoFuels.create(trip);
            }

            helper.close();
        } catch (SQLException e) {
            Logger.Error("Error when inserting Trips to DB", e);
        }
    }

    public static List<Trip> getAllTrips(Context context) {
        List<Trip> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Trip, String> daoFuels = helper.getTripDao();

            result = daoFuels.queryForAll();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting Trips to DB", e);
        }

        return result;
    }

    public static Trip getTripById(Context context, int id) {
        Trip result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Trip, String> daoFuels = helper.getTripDao();

            QueryBuilder<Trip, String> qbUsers = daoFuels.queryBuilder();
            qbUsers.where().eq(Trip.FIELD_TRIP_ID, id);

            result = qbUsers.queryForFirst();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting Trip by model to DB", e);
        }

        return result;
    }
}
