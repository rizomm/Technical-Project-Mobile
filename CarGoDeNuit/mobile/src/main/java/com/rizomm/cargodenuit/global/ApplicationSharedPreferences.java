package com.rizomm.cargodenuit.global;

import android.content.Context;
import android.content.SharedPreferences;

public class ApplicationSharedPreferences {
	// Constants
	// --------------------------------------------------------------------------------------------
	private static ApplicationSharedPreferences instance;
	
	private static final String PREFS_NAME = "ApplicationPreferencesFile";
	
	private static final String IS_FIRST_LAUNCH = "isFirstLaunch";

    // Members
    // --------------------------------------------------------------------------------------------
	private static SharedPreferences sApplicationSharedPreferences;
	private static SharedPreferences.Editor sEditor;
	
	// Constructor
	// --------------------------------------------------------------------------------------------
	private ApplicationSharedPreferences(Context context){
		sApplicationSharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		sEditor = sApplicationSharedPreferences.edit();
	}

	// Singleton
	// --------------------------------------------------------------------------------------------
	public static ApplicationSharedPreferences getInstance(Context context) {
		if (instance == null) {
			instance =  new ApplicationSharedPreferences(context);
		}
		return instance;
	}

	// Getters & Setters
    // --------------------------------------------------------------------------------------------
	public boolean isFirstLaunch() {
		return sApplicationSharedPreferences.getBoolean(IS_FIRST_LAUNCH, true);
	}
	public void setFirstLaunch(boolean isfirstlaunch){
		sEditor.putBoolean(IS_FIRST_LAUNCH, isfirstlaunch);
		sEditor.commit();
	}
}
