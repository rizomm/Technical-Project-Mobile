package com.rizomm.cargodenuit.controllers.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.rizomm.cargodenuit.R;
import com.rizomm.cargodenuit.controllers.fragment.LoginFragment;

public class LoginActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_login_container, new LoginFragment()).commit();
    }
}
