package com.rizomm.cargodenuit.services.wear;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;


import com.rizomm.cargodenuit.common.global.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.rizomm.cargodenuit.common.helpers.Logger;

import java.util.Date;

import de.greenrobot.event.EventBus;

public class WearDataService extends Service {

    // Constants
    // --------------------------------------------------------------------------------------------

    // Members
    // --------------------------------------------------------------------------------------------
    private GoogleApiClient mGoogleApiClient;

    // Life cycle
    // --------------------------------------------------------------------------------------------
    @Override
    public void onCreate() {
        super.onCreate();
        Logger.Debug("Service running!");
        initGoogleApiClient();
        //EventBus.getDefault().register(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.Debug("Service closed!");
        mGoogleApiClient.disconnect();
        EventBus.getDefault().unregister(this);
    }

    // Wear methods
    // --------------------------------------------------------------------------------------------
  /*  private void sendMessageToWatch(String airportCode, long degrees, long distance, long timeToArrive, String previousStep, String nextStep) {
        PutDataMapRequest mapRequest = PutDataMapRequest.create(Constants.TRIP_DATA_ITEM_PATH);
        mapRequest.getDataMap().putString(Constants.TRIP_KEY_AIRPORT_CODE, airportCode);
        mapRequest.getDataMap().putLong(Constants.TRIP_KEY_DEGREES, degrees);
        mapRequest.getDataMap().putLong(Constants.TRIP_KEY_DISTANCE, distance);
        mapRequest.getDataMap().putLong(Constants.TRIP_KEY_TIME_TO_ARRIVE, timeToArrive);
        mapRequest.getDataMap().putLong(Constants.TRIP_KEY_TIMESTAMP, new Date().getTime());
        mapRequest.getDataMap().putString(Constants.TRIP_KEY_PREVIOUS_STEP, previousStep);
        mapRequest.getDataMap().putString(Constants.TRIP_KEY_NEXT_STEP, nextStep);

        PutDataRequest request = mapRequest.asPutDataRequest();

        Wearable.DataApi.putDataItem(mGoogleApiClient, request);
    } */

    private void sendNotificationToOpenApp() {

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
                @Override
                public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                    for (Node node : nodes.getNodes()) {
                        Logger.Debug("Sending message to " + node.getId() + " !");
                        Wearable.MessageApi.sendMessage(
                                mGoogleApiClient, node.getId(), Constants.START_APP_MESSAGE_PATH, null).setResultCallback(

                                new ResultCallback<MessageApi.SendMessageResult>() {
                                    @Override
                                    public void onResult(MessageApi.SendMessageResult sendMessageResult) {

                                        if (!sendMessageResult.getStatus().isSuccess()) {
                                            Log.e("TAG", "Failed to send message with status code: " + sendMessageResult.getStatus().getStatusCode());
                                        }
                                    }
                                }
                        );
                    }
                }
            });

        }

    }

    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Logger.Debug("onConnected: " + connectionHint);
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        Logger.Debug("onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Logger.Debug("onConnectionFailed: " + result);
                    }
                })
                        // Request access only to the Wearable API
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();
    }

    // EventBus
    // --------------------------------------------------------------------------------------------


}
