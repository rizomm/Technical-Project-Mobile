package com.rizomm.cargodenuit.providers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rizomm.cargodenuit.common.helpers.Logger;
import com.rizomm.cargodenuit.common.models.Music;
import com.rizomm.cargodenuit.database.ORMLiteDatabaseHelper;

import java.sql.SQLException;
import java.util.List;

public class MusicProvider {

    public static void getMusics(Context context) {
        try {
            List<Music> toInsert = null; //Todo : link with Web Service
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Music, String> daoMusics = helper.getMusicDao();

            for (Music music: toInsert) {
                daoMusics.create(music);
            }

            helper.close();
        } catch (SQLException e) {
            Logger.Error("Error when inserting Musics to DB", e);
        }
    }

    public static List<Music> getAllMusics(Context context) {
        List<Music> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Music, String> daoMusics = helper.getMusicDao();

            result = daoMusics.queryForAll();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting Musics to DB", e);
        }

        return result;
    }

    public static Music getMusicById(Context context, int id) {
        Music result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Music, String> daoMusics = helper.getMusicDao();

            QueryBuilder<Music, String> qbUsers = daoMusics.queryBuilder();
            qbUsers.where().eq(Music.FIELD_MUSIC_ID, id);

            result = qbUsers.queryForFirst();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting Music by model to DB", e);
        }

        return result;
    }
}
