package com.rizomm.cargodenuit.providers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rizomm.cargodenuit.common.helpers.Logger;
import com.rizomm.cargodenuit.common.models.UserType;
import com.rizomm.cargodenuit.database.ORMLiteDatabaseHelper;

import java.sql.SQLException;
import java.util.List;

public class UserTypeProvider {

    public static void getUserTypes(Context context) {
        try {
            List<UserType> toInsert = null; //Todo : link with Web Service
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<UserType, String> daoUserTypes = helper.getUserTypeDao();

            for (UserType userType: toInsert) {
                daoUserTypes.create(userType);
            }

            helper.close();
        } catch (SQLException e) {
            Logger.Error("Error when inserting UserTypes to DB", e);
        }
    }

    public static List<UserType> getAllUserTypes(Context context) {
        List<UserType> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<UserType, String> daoUserTypes = helper.getUserTypeDao();

            result = daoUserTypes.queryForAll();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting UserTypes to DB", e);
        }

        return result;
    }

    public static UserType getUserTypeById(Context context, int id) {
        UserType result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<UserType, String> daoUserTypes = helper.getUserTypeDao();

            QueryBuilder<UserType, String> qbUsers = daoUserTypes.queryBuilder();
            qbUsers.where().eq(UserType.FIELD_USER_TYPE_ID, id);

            result = qbUsers.queryForFirst();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting UserType by model to DB", e);
        }

        return result;
    }
}
