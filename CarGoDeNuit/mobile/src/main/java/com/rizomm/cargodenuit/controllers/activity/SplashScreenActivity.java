package com.rizomm.cargodenuit.controllers.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.rizomm.cargodenuit.R;
import com.rizomm.cargodenuit.controllers.fragment.SplashScreenFragment;

public class SplashScreenActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_splashscreen_container, new SplashScreenFragment()).commit();
    }
}
