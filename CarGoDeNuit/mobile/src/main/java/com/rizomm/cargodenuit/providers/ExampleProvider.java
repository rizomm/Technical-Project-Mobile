package com.rizomm.cargodenuit.providers;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import com.rizomm.cargodenuit.common.models.Example;
import com.rizomm.cargodenuit.common.models.FlightInfo;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class ExampleProvider {

    // Constants
    // --------------------------------------------------------------------------------------------
    private static final String LOG_TAG = ExampleProvider.class.getSimpleName();

    // Members
    // --------------------------------------------------------------------------------------------
    private ArrayList<Example> mCurrentTrip = new ArrayList<Example>();
    private int mStep = 0;
    private boolean isTripStarted = false;

    private FlightInfo mFlightInfo = new FlightInfo();
    private Location mUserLocation;
    private Context mContext;

    // Singleton
    // --------------------------------------------------------------------------------------------
    private static ExampleProvider sSingleton;

    public static ExampleProvider getInstance(Context context) {
        if (sSingleton == null) {
            sSingleton = new ExampleProvider(context);
        }

        return sSingleton;
    }

    // Constructor
    // --------------------------------------------------------------------------------------------
    public ExampleProvider(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        mUserLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        mContext = context;

        EventBus.getDefault().register(this);
    }

    // Implementation
    // --------------------------------------------------------------------------------------------
    private void goToStep(int position) {
        if (mCurrentTrip.size() > position && position >= 0) {
            mStep = position;
            refreshFlightInfo();
        }
    }

    private void refreshFlightInfo() {
     /*   mFlightInfo.setAirportCode(mCurrentTrip.get(mStep).getCode());
        mFlightInfo.setDegrees(BearingHelper.calculateBearing(mUserLocation.getLatitude(), mUserLocation.getLongitude(), mCurrentTrip.get(mStep).getLatitude(), mCurrentTrip.get(mStep).getLongitude()));
        mFlightInfo.setDistance(DistanceHelper.getDistanceInNauticalMiles(mUserLocation.getLatitude(), mUserLocation.getLongitude(), mCurrentTrip.get(mStep).getLatitude(), mCurrentTrip.get(mStep).getLongitude()));
        int speed = Math.round(mUserLocation.getSpeed());
        if (speed != 0) {
            mFlightInfo.setTimeToArrive(DistanceHelper.getDistanceInMeters(mUserLocation.getLatitude(), mUserLocation.getLongitude(), mCurrentTrip.get(mStep).getLatitude(), mCurrentTrip.get(mStep).getLongitude())/speed);
        } else {
            mFlightInfo.setTimeToArrive(0);
        }
        if (mStep != 0) {
            mFlightInfo.setPreviousStep(mCurrentTrip.get(mStep - 1).getCode());
        } else {
            mFlightInfo.setPreviousStep("");
        }
        if (mCurrentTrip.size() > mStep + 1) {
            mFlightInfo.setNextStep(mCurrentTrip.get(mStep + 1).getCode());
        } else {
            mFlightInfo.setNextStep("");
        }

        EventBus.getDefault().post(new OnTripChangedEvent(mFlightInfo));
        Log.d(LOG_TAG, "--- Trip changed! ---");*/
    }

    // Public methods
    // --------------------------------------------------------------------------------------------
    /**
     * Starts the trip
     *
     * @return true if the trip has been successfully launched, false otherwise
     */
    public boolean startTrip() {
        if (mCurrentTrip.size() > 0) {
            isTripStarted = true;
          //  EventBus.getDefault().post(new OnTripStartedEvent());
            goToStep(0);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Stops the trip
     */
    public void stopTrip() {
        isTripStarted = false;
      //  EventBus.getDefault().post(new OnTripEndedEvent());
    }

    /**
     * Retrieves list of airports for current trip
     */
    public ArrayList<Example> getCurrentTrip() {
        return mCurrentTrip;
    }

    /**
     * Retrieves current flight info
     */
    public FlightInfo getFlightInfo() {
        return mFlightInfo;
    }

    /**
     * Adds an airport as last step of the current trip
     *
     * @param example Airport to be added
     */
    public boolean addAirportToCurrentTrip(Example example) {
        return mCurrentTrip.add(example);
    }

    public boolean removeAirportFromCurrentTrip(Example example) {
        return mCurrentTrip.remove(example);
    }

    public void clearCurrentTrip() {
        mCurrentTrip.clear();
    }

    /**
     * Move to previous step in the current trip
     */
    public void goToNextStep() {
        goToStep(mStep + 1);
    }

    /**
     * Move to next step in the current trip
     */
    public void goToPreviousStep() {
        goToStep(mStep - 1);
    }

    // EventBus
    // --------------------------------------------------------------------------------------------
    /*
    public void onEvent(OnLocationEvent event) {
        mUserLocation = event.getLocation();
        if (isTripStarted) {
            if (DistanceHelper.isDistanceInNauticalMilesLowerThan(mUserLocation.getLatitude(), mUserLocation.getLongitude(), mCurrentTrip.get(mStep).getLatitude(), mCurrentTrip.get(mStep).getLongitude(), 3)) {
                goToNextStep();
            } else {
                refreshFlightInfo();
            }
        }
    }

    public void onEvent(OnWearChangeTripEvent event) {
        if (event.isNext()) {
            goToNextStep();
        } else {
            goToPreviousStep();
        }
    }*/
}
