    package com.rizomm.cargodenuit.providers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rizomm.cargodenuit.common.helpers.Logger;
import com.rizomm.cargodenuit.common.models.Car;
import com.rizomm.cargodenuit.database.ORMLiteDatabaseHelper;

import java.sql.SQLException;
import java.util.List;

public class CarProvider {

    public static void getCars(Context context) {
        try {
            List<Car> toInsert = null; //Todo : link with Web Service
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Car, String> daoCars = helper.getCarDao();

            for (Car car: toInsert) {
                daoCars.create(car);
            }

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when inserting cars to DB", e);
        }
    }

    public static List<Car> getAllCars(Context context) {
        List<Car> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Car, String> daoCars = helper.getCarDao();

            result = daoCars.queryForAll();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting cars to DB", e);
        }

        return result;
    }

    public static List<Car> getCarByModel(Context context, String type) {
        List<Car> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Car, String> daoCars = helper.getCarDao();

            QueryBuilder<Car, String> qbUsers = daoCars.queryBuilder();
            qbUsers.where().eq(Car.FIELD_CAR_MODEL_ID, type);

            result = qbUsers.query();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting cars by model to DB", e);
        }

        return result;
    }

    public static Car getCarById(Context context, int id) {
        Car result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Car, String> daoCars = helper.getCarDao();

            QueryBuilder<Car, String> qbUsers = daoCars.queryBuilder();
            qbUsers.where().eq(Car.FIELD_CAR_MODEL_ID, id);

            result = qbUsers.queryForFirst();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting car by id to DB", e);
        }

        return result;
    }
}
