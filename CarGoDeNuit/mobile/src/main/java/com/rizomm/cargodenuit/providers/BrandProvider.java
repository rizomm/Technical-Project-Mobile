package com.rizomm.cargodenuit.providers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rizomm.cargodenuit.common.helpers.Logger;
import com.rizomm.cargodenuit.common.models.Brand;
import com.rizomm.cargodenuit.database.ORMLiteDatabaseHelper;

import java.sql.SQLException;
import java.util.List;

public class BrandProvider {

    public static void getBrands(Context context) {
        try {
            List<Brand> toInsert = null; //Todo : link with Web Service
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Brand, String> daoBrands = helper.getBrandDao();

            for (Brand brand: toInsert) {
                daoBrands.create(brand);
            }

            helper.close();
        } catch (SQLException e) {
            Logger.Error("Error when inserting brands to DB", e);
        }
    }

    public static List<Brand> getAllBrands(Context context) {
        List<Brand> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Brand, String> daoBrands = helper.getBrandDao();

            result = daoBrands.queryForAll();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting brands to DB", e);
        }

        return result;
    }

    public static Brand getBrandById(Context context, int id) {
        Brand result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Brand, String> daoBrands = helper.getBrandDao();

            QueryBuilder<Brand, String> qbUsers = daoBrands.queryBuilder();
            qbUsers.where().eq(Brand.FIELD_BRAND_ID, id);

            result = qbUsers.queryForFirst();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting brand by model to DB", e);
        }

        return result;
    }
}
