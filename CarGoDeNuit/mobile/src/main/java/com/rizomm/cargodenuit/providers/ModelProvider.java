package com.rizomm.cargodenuit.providers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rizomm.cargodenuit.common.helpers.Logger;
import com.rizomm.cargodenuit.common.models.Model;
import com.rizomm.cargodenuit.database.ORMLiteDatabaseHelper;

import java.sql.SQLException;
import java.util.List;

public class ModelProvider {

    public static void getModels(Context context) {
        try {
            List<Model> toInsert = null; //Todo : link with Web Service
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Model, String> daoModels = helper.getModelDao();

            for (Model model: toInsert) {
                daoModels.create(model);
            }

            helper.close();
        } catch (SQLException e) {
            Logger.Error("Error when inserting Models to DB", e);
        }
    }

    public static List<Model> getAllModels(Context context) {
        List<Model> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Model, String> daoModels = helper.getModelDao();

            result = daoModels.queryForAll();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting Models to DB", e);
        }

        return result;
    }

    public static Model getModelById(Context context, int id) {
        Model result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<Model, String> daoModels = helper.getModelDao();

            QueryBuilder<Model, String> qbUsers = daoModels.queryBuilder();
            qbUsers.where().eq(Model.FIELD_MODEL_ID, id);

            result = qbUsers.queryForFirst();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting Model by model to DB", e);
        }

        return result;
    }
}
