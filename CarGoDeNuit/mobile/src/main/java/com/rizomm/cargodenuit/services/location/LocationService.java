package com.rizomm.cargodenuit.services.location;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.rizomm.cargodenuit.common.events.OnLocationEvent;
import com.rizomm.cargodenuit.common.helpers.Logger;

import de.greenrobot.event.EventBus;

public class LocationService extends Service {

    // Constants
    // --------------------------------------------------------------------------------------------
    private static final long TIME_INTERVAL         = 5 * 1000;
    private static final float DISTANCE_INTERVAL    = 5;

    // Members
    // --------------------------------------------------------------------------------------------
    private LocationManager mLocationManager;
    private MyLocationListener mLocationListener;
    private String mProvider;
    private Location mLatestLocation;

    private Handler handler;

    // Life cycle
    // --------------------------------------------------------------------------------------------
    @Override
    public void onCreate() {
        super.onCreate();
        Logger.Debug("Service running!");
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mLocationListener = new MyLocationListener();
        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mProvider = LocationManager.GPS_PROVIDER;
        } else {
            mProvider = LocationManager.NETWORK_PROVIDER;
        }
        handler = new Handler(Looper.getMainLooper());
        startOrRestartLocationTracking();
    }

    @Override
    public void onDestroy() {
        Logger.Debug("Service closed!");
        super.onDestroy();
        stopLocationTracking();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // Implementation
    // --------------------------------------------------------------------------------------------
    private void startOrRestartLocationTracking() {
        Logger.Debug("StartTracking with provider : " + mProvider);
        handler.post(new Runnable() {
            @Override
            public void run() {
                mLocationManager.removeUpdates(mLocationListener);
                mLocationManager.requestLocationUpdates(mProvider, TIME_INTERVAL, DISTANCE_INTERVAL, mLocationListener);
            }
        });
    }

    private void stopLocationTracking() {
        mLocationManager.removeUpdates(mLocationListener);
    }

    public Location getLatestLocation() {
        return mLatestLocation;
    }

    // Location Listener
    // --------------------------------------------------------------------------------------------
    public class MyLocationListener implements LocationListener {
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Logger.Debug("onStatusChanged");
        }

        @Override
        public void onLocationChanged(Location location) {
            Logger.Debug("New location!");
            mLatestLocation = location;
            EventBus.getDefault().post(new OnLocationEvent(location));
        }

        @Override
        public void onProviderDisabled(String provider) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                mProvider = LocationManager.NETWORK_PROVIDER;
            }
            startOrRestartLocationTracking();
        }

        @Override
        public void onProviderEnabled(String provider) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                mProvider = LocationManager.GPS_PROVIDER;
            }
            startOrRestartLocationTracking();
        }
    }
}
