package com.rizomm.cargodenuit.providers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rizomm.cargodenuit.common.helpers.Logger;
import com.rizomm.cargodenuit.common.models.User;
import com.rizomm.cargodenuit.database.ORMLiteDatabaseHelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserProvider {
    private static final ArrayList<User> mUsers = new ArrayList<User>();

    public static void getUsers(Context context) {
        User userTest = new User(
                "Pierrick",
                "Druart",
                "7",
                "rue de toul",
                "59000",
                "Lille",
                "",
                "pierrick.druart@gmail.com",
                "123456",
                "0610922343",
                "01/01/2015",
                "27/02/2015",
                true,
                true,
                "123",
                1,
                1);
        mUsers.add(userTest);
        try {
            List<User> toInsert = mUsers; //Todo : link with Web Service
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<User, String> daoUsers = helper.getUserDao();

            for (User user: toInsert) {
                daoUsers.create(user);
            }

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when inserting users to DB", e);
        }
    }

    public static List<User> getAllUsers(Context context) {
        List<User> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<User, String> daoUsers = helper.getUserDao();

            result = daoUsers.queryForAll();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting users to DB", e);
        }

        return result;
    }

    public static List<User> getUsersByType(Context context, String type) {
        List<User> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<User, String> daoUsers = helper.getUserDao();

            QueryBuilder<User, String> qbUsers = daoUsers.queryBuilder();
            qbUsers.where().eq(User.FIELD_USER_TYPE, type);

            result = qbUsers.query();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting users by type to DB", e);
        }

        return result;
    }

    public static User getUserById(Context context, int id) {
        User result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<User, String> daoUsers = helper.getUserDao();

            QueryBuilder<User, String> qbUsers = daoUsers.queryBuilder();
            qbUsers.where().eq(User.FIELD_USER_ID, id);

            result = qbUsers.queryForFirst();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting user by id to DB", e);
        }

        return result;
    }
}
