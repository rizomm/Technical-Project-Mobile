package com.rizomm.cargodenuit.services.rest;

import com.rizomm.cargodenuit.common.models.User;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;

public interface UserService {
    public static final String CMD_GET_BY_USER_TYPE    = "getByUserType";

    @GET("/users/{cmd}/{search}")
    List<User> listUsers(
            @Path("cmd")    String      cmd,
            @Path("search") String      search
    );

    @GET("/users")
    List<User> listUsersAll();
}
