package com.rizomm.cargodenuit.controllers.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.rizomm.cargodenuit.R;
import com.rizomm.cargodenuit.controllers.activity.MainActivity;
import com.rizomm.cargodenuit.controllers.activity.SplashScreenActivity;
import com.rizomm.cargodenuit.global.ApplicationSharedPreferences;
import com.rizomm.cargodenuit.providers.UserProvider;

public class LoginFragment extends Fragment {
    private EditText    inputLogin;
    private EditText    inputPassword;
    private Button      btnConnection;

    // Members
    // --------------------------------------------------------------------------------------------
    private void initializeViews(View v) {
        inputLogin      = (EditText) v.findViewById(R.id.inputLogin);
        inputPassword   = (EditText) v.findViewById(R.id.inputPassword);
        btnConnection   = (Button) v.findViewById(R.id.btnConnection);
    }

    // Life cycle
    // --------------------------------------------------------------------------------------------
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);

       /* if (!ApplicationSharedPreferences.getInstance(getActivity().getApplicationContext()).isFirstLaunch()) {
            showSplashscreenActivity();
        } */

        initializeViews(v);
        getActivity().runOnUiThread(refreshRunnable);

        btnConnection.setOnClickListener(onClickListener);
        return v;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnConnection:
                    new ConnectionTask().execute();
                    break;
                default:
                    return;
            }
        }
    };

    // Views navigation
    // --------------------------------------------------------------------------------------------
    private void showSplashscreenActivity() {
        startActivity(new Intent(getActivity().getApplicationContext(), SplashScreenActivity.class));
        getActivity().finish();
    }

    private Runnable refreshRunnable = new Runnable() {
        @Override
        public void run() {
            //TODO : set view text
        }
    };

    // AsyncTask
    // --------------------------------------------------------------------------------------------
    private class ConnectionTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            //TODO : import Users and others Data
            UserProvider.getUsers(getActivity().getApplicationContext());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            showSplashscreenActivity();
        }

    }
}
