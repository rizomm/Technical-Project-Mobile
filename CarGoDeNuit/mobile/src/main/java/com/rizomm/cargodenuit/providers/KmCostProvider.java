package com.rizomm.cargodenuit.providers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rizomm.cargodenuit.common.helpers.Logger;
import com.rizomm.cargodenuit.common.models.KmCost;
import com.rizomm.cargodenuit.database.ORMLiteDatabaseHelper;

import java.sql.SQLException;
import java.util.List;

public class KmCostProvider {

    public static void getKmCosts(Context context) {
        try {
            List<KmCost> toInsert = null; //Todo : link with Web Service
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<KmCost, String> daoKmCosts = helper.getKmCostDao();

            for (KmCost kmCost: toInsert) {
                daoKmCosts.create(kmCost);
            }

            helper.close();
        } catch (SQLException e) {
            Logger.Error("Error when inserting KmCosts to DB", e);
        }
    }

    public static List<KmCost> getAllKmCosts(Context context) {
        List<KmCost> result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<KmCost, String> daoKmCosts = helper.getKmCostDao();

            result = daoKmCosts.queryForAll();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting KmCosts to DB", e);
        }

        return result;
    }

    public static KmCost getKmCostById(Context context, int id) {
        KmCost result = null;

        try {
            ORMLiteDatabaseHelper helper = ORMLiteDatabaseHelper.getHelper(context);
            Dao<KmCost, String> daoKmCosts = helper.getKmCostDao();

            QueryBuilder<KmCost, String> qbUsers = daoKmCosts.queryBuilder();
            qbUsers.where().eq(KmCost.FIELD_KM_COST_ID, id);

            result = qbUsers.queryForFirst();

            helper.close();
        } catch (SQLException e) {
            Logger.Error( "Error when getting KmCost by model to DB", e);
        }

        return result;
    }
}
