package com.rizomm.cargodenuit.services;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import de.greenrobot.event.EventBus;

public class NotificationUpdateService extends WearableListenerService {

    // Constants
    // --------------------------------------------------------------------------------------------
    private static final String LOG_TAG = NotificationUpdateService.class.getSimpleName();

    // Members
    // --------------------------------------------------------------------------------------------


    // Implementation
    // --------------------------------------------------------------------------------------------
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (null != intent) {
            String action = intent.getAction();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        for (DataEvent dataEvent : dataEvents) {
            Log.d(LOG_TAG, "Received data event!");
            if (dataEvent.getType() == DataEvent.TYPE_CHANGED) {
               /* if ((Constants.TRIP_DATA_ITEM_PATH.equals(dataEvent.getDataItem().getUri().getPath()))) {
                    DataMap data = DataMapItem.fromDataItem(dataEvent.getDataItem()).getDataMap();
                    if (data.containsKey(Constants.TRIP_KEY_DEGREES) && data.containsKey(Constants.TRIP_KEY_DISTANCE)) {
                        mFlightInfo.setAirportCode(data.getString(Constants.TRIP_KEY_AIRPORT_CODE));
                        mFlightInfo.setDegrees(data.getLong(Constants.TRIP_KEY_DEGREES));
                        mFlightInfo.setDistance(data.getLong(Constants.TRIP_KEY_DISTANCE));
                        mFlightInfo.setTimeToArrive(data.getLong(Constants.TRIP_KEY_TIME_TO_ARRIVE));
                        mFlightInfo.setPreviousStep(data.getString(Constants.TRIP_KEY_PREVIOUS_STEP));
                        mFlightInfo.setNextStep(data.getString(Constants.TRIP_KEY_NEXT_STEP));
                        EventBus.getDefault().post(new OnRefreshWearEvent(mFlightInfo));
                    }
                } */
            }
        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
       /* if (messageEvent.getPath().equals(Constants.START_APP_MESSAGE_PATH)) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }*/
    }

}