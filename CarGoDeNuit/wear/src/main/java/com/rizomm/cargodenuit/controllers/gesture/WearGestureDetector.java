package com.rizomm.cargodenuit.controllers.gesture;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

public class WearGestureDetector extends GestureDetector.SimpleOnGestureListener {

    // Constants
    // --------------------------------------------------------------------------------------------
    private static final String LOG_TAG = WearGestureDetector.class.getSimpleName();

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

    // Members
    // --------------------------------------------------------------------------------------------
    private WearDetectionCallback listener;

    // Constructor
    // --------------------------------------------------------------------------------------------
    public WearGestureDetector(WearDetectionCallback listener) {
        this.listener = listener;
    }

    // Implementation
    // --------------------------------------------------------------------------------------------
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                return false;
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                if (listener != null) {
                    listener.onSwipeLeft();
                }
            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                if (listener != null) {
                    listener.onSwipeRight();
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error when checking for swipe movement", e);
        }
        return false;
    }

    @Override
    public void onLongPress(MotionEvent event) {
        listener.onQuit();
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        listener.switchShowingDistance();
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    // Callbacks
    // --------------------------------------------------------------------------------------------
    public interface WearDetectionCallback {
        void onSwipeLeft();
        void onSwipeRight();
        void switchShowingDistance();
        void onQuit();
    }

    /* DOUBLE TAP GESTURES

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTapEvent: " + event.toString());
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTap: " + event.toString());
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapConfirmed: " + event.toString());
        return true;
    }

    */
}
