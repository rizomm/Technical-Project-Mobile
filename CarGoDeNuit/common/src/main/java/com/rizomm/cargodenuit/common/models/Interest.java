package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "interest")
public class Interest {
    public static final String FIELD_INTEREST_ID    = "interestId";
    public static final String FIELD_INTEREST_NAME  = "interestName";

    @DatabaseField(columnName = FIELD_INTEREST_ID)
    private int id;

    @DatabaseField(columnName = FIELD_INTEREST_NAME)
    private String name;

    public Interest() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
