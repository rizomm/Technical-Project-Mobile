package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "brand")
public class Brand {
    public static final String FIELD_BRAND_ID = "brandId";
    public static final String FIELD_BRAND_NAME = "brandName";

    @DatabaseField(columnName = FIELD_BRAND_ID)
    private int id;

    @DatabaseField(columnName = FIELD_BRAND_NAME)
    private String name;

    public Brand() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
