package com.rizomm.cargodenuit.common.global;

public class Settings {
	// Constants
	// -----------------------------------------------------------------------------------------------------
	
	// Application Settings
	// ------------------------------------------------------------------------------------------------------------------------
	public static boolean isDebugEnvironment = true;
	
	// Webservice Settings
	// ------------------------------------------------------------------------------------------------------------------------
    public static final String BASE_URL = "";
    public static final String URL_SERVICE = "";
	
	public static final String APPLICATION_NAME = "Cargo De Nuit";
	public static final String APPLICATION_VERSION = "1.0";
}
