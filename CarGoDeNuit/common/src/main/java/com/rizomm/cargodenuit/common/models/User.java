package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "user")
public class User {
    public static final String FIELD_USER_ID                = "userId";
    public static final String FIELD_USER_FIRST_NAME        = "userFirstName";
    public static final String FIELD_USER_LAST_NAME         = "userLastName";
    public static final String FIELD_USER_ADDRESS_NUMBER    = "userAddressNumber";
    public static final String FIELD_USER_ADDRESS_STREET    = "userAddressStreet";
    public static final String FIELD_USER_ADDRESS_CC        = "userAddressCountryCode";
    public static final String FIELD_USER_ADDRESS_TOWN      = "userAddressTown";
    public static final String FIELD_USER_ADDRESS_OTHER     = "userAddressOther";
    public static final String FIELD_USER_MAIL              = "userMail";
    public static final String FIELD_USER_PASSWORD          = "userPassword";
    public static final String FIELD_USER_PHONE             = "userPhone";
    public static final String FIELD_USER_MARK              = "userMark";
    public static final String FIELD_USER_DATE_SUBSCRIBE    = "userDateSubscribe";
    public static final String FIELD_USER_LAST_CONNECTION   = "userLAstConnection";
    public static final String FIELD_USER_SMOKING           = "smoker";
    public static final String FIELD_USER_TALKATIVE         = "userTalkative";
    public static final String FIELD_USER_LISTEN_MUSIC      = "userListenMusic";
    public static final String FIELD_USER_DRIVER_LICENSE    = "userDriverLicense";
    public static final String FIELD_USER_TYPE              = "userType_id";
    public static final String FIELD_USER_CAR_ID            = "userCarID";

    @DatabaseField(columnName = FIELD_USER_ID)
    private int id;

    @DatabaseField(columnName = FIELD_USER_FIRST_NAME)
    private String firstname;

    @DatabaseField(columnName = FIELD_USER_LAST_NAME)
    private String lastname;

    @DatabaseField(columnName = FIELD_USER_ADDRESS_NUMBER)
    private String adressNumber;

    @DatabaseField(columnName = FIELD_USER_ADDRESS_STREET)
    private String adressStreet;

    @DatabaseField(columnName = FIELD_USER_ADDRESS_CC)
    private String adressZipCode;

    @DatabaseField(columnName = FIELD_USER_ADDRESS_TOWN)
    private String adressCity;

    @DatabaseField(columnName = FIELD_USER_ADDRESS_OTHER)
    private String adressComplement;

    @DatabaseField(columnName = FIELD_USER_MAIL)
    private String email;

    @DatabaseField(columnName = FIELD_USER_PASSWORD)
    private String password;

    @DatabaseField(columnName = FIELD_USER_PHONE)
    private String phone;

    @DatabaseField(columnName = FIELD_USER_MARK)
    private int mark;

    @DatabaseField(columnName = FIELD_USER_DATE_SUBSCRIBE)
    private String registrationDate;

    @DatabaseField(columnName = FIELD_USER_LAST_CONNECTION)
    private String lastConnexion;

    @DatabaseField(columnName = FIELD_USER_SMOKING)
    private boolean smoker;

    @DatabaseField(columnName = FIELD_USER_TALKATIVE)
    private boolean talker;

    @DatabaseField(columnName = FIELD_USER_LISTEN_MUSIC)
    private boolean listenMusic;

    @DatabaseField(columnName = FIELD_USER_DRIVER_LICENSE)
    private String drivingLicenseNumber;

    @DatabaseField(columnName = FIELD_USER_TYPE)
    private int userType_id;

    @DatabaseField(columnName = FIELD_USER_CAR_ID)
    private int car_id;

    public User() {
    }

    public User(String firstName, String secondName, String addressNumber, String addressStreet, String addressCountryCode, String addressTown, String addressOther, String mail, String password, String phone, String dateSubscribe, String dateLastConnection, boolean smoking, boolean talkative, String driverLicense, int userType, int carId) {
        this.firstname = firstName;
        this.lastname = secondName;
        this.adressNumber = addressNumber;
        this.adressStreet = addressStreet;
        this.adressZipCode = addressCountryCode;
        this.adressCity = addressTown;
        this.adressComplement = addressOther;
        this.email = mail;
        this.password = password;
        this.phone = phone;
        this.registrationDate = dateSubscribe;
        this.lastConnexion = dateLastConnection;
        this.smoker = smoking;
        this.talker = talkative;
        this.drivingLicenseNumber = driverLicense;
        this.userType_id = userType;
        this.car_id = carId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAdressNumber() {
        return adressNumber;
    }

    public void setAdressNumber(String adressNumber) {
        this.adressNumber = adressNumber;
    }

    public String getAdressStreet() {
        return adressStreet;
    }

    public void setAdressStreet(String adressStreet) {
        this.adressStreet = adressStreet;
    }

    public String getAdressZipCode() {
        return adressZipCode;
    }

    public void setAdressZipCode(String adressZipCode) {
        this.adressZipCode = adressZipCode;
    }

    public String getAdressCity() {
        return adressCity;
    }

    public void setAdressCity(String adressCity) {
        this.adressCity = adressCity;
    }

    public String getAdressComplement() {
        return adressComplement;
    }

    public void setAdressComplement(String adressComplement) {
        this.adressComplement = adressComplement;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getLastConnexion() {
        return lastConnexion;
    }

    public void setLastConnexion(String lastConnexion) {
        this.lastConnexion = lastConnexion;
    }

    public boolean isSmoker() {
        return smoker;
    }

    public void setSmoker(boolean smoker) {
        this.smoker = smoker;
    }

    public boolean isTalker() {
        return talker;
    }

    public void setTalker(boolean talker) {
        this.talker = talker;
    }

    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }

    public int getUserType_id() {
        return userType_id;
    }

    public void setUserType_id(int userType_id) {
        this.userType_id = userType_id;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public boolean isListenMusic() {
        return listenMusic;
    }

    public void setListenMusic(boolean listenMusic) {
        this.listenMusic = listenMusic;
    }
}
