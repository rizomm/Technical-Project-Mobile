package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "kmCost")
public class KmCost {
    public static final String FIELD_KM_COST_ID         = "kmCostId";
    public static final String FIELD_KM_COST_COST       = "kmCostCost";
    public static final String FIELD_KM_COST_FUEL_ID    = "kmCostFueldId";

    @DatabaseField(columnName = FIELD_KM_COST_ID)
    private int id;

    @DatabaseField(columnName = FIELD_KM_COST_COST)
    private int cost;

    @DatabaseField(columnName = FIELD_KM_COST_FUEL_ID)
    private int fuel_id;

    public KmCost() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getFuel_id() {
        return fuel_id;
    }

    public void setFuel_id(int fuel_id) {
        this.fuel_id = fuel_id;
    }
}
