package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "booking")
public class Booking {
    public static final String FIELD_BOOKING_ID                 = "bookingId";
    public static final String FIELD_BOOKING_NUMBER_OF_SEAT     = "bookingNumberOfSeat";
    public static final String FIELD_BOOKING_LATITUDE_START     = "bookingLatitudeStart";
    public static final String FIELD_BOOKING_LONGITUDE_START    = "bookingLongitudeStart";
    public static final String FIELD_BOOKING_LATITUDE_END       = "bookingLatitudeEnd";
    public static final String FIELD_BOOKING_LONGITUDE_END      = "bookingLongitudeEnd";
    public static final String FIELD_BOOKING_START_DATE         = "bookingStartDate";
    public static final String FIELD_BOOKING_END_DATE           = "bookingEndDate";
    public static final String FIELD_BOOKING_TRIP_ID            = "bookingTripId";
    public static final String FIELD_BOOKING_USER_ID            = "bookingUserId";

    @DatabaseField(columnName = FIELD_BOOKING_ID)
    private int id;

    @DatabaseField(columnName = FIELD_BOOKING_NUMBER_OF_SEAT)
    private String numberOfSeat;

    @DatabaseField(columnName = FIELD_BOOKING_LATITUDE_START)
    private String latitudeStart;

    @DatabaseField(columnName = FIELD_BOOKING_LONGITUDE_START)
    private String longitudeStart;

    @DatabaseField(columnName = FIELD_BOOKING_LATITUDE_END)
    private String latitudeEnd;

    @DatabaseField(columnName = FIELD_BOOKING_LONGITUDE_END)
    private String longitudeEnd;

    @DatabaseField(columnName = FIELD_BOOKING_START_DATE)
    private String startDate;

    @DatabaseField(columnName = FIELD_BOOKING_END_DATE)
    private String endDate;

    @DatabaseField(columnName = FIELD_BOOKING_TRIP_ID)
    private int trip_id;

    @DatabaseField(columnName = FIELD_BOOKING_USER_ID)
    private int user_id;

    public Booking() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(String numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    public String getLatitudeStart() {
        return latitudeStart;
    }

    public void setLatitudeStart(String latitudeStart) {
        this.latitudeStart = latitudeStart;
    }

    public String getLongitudeStart() {
        return longitudeStart;
    }

    public void setLongitudeStart(String longitudeStart) {
        this.longitudeStart = longitudeStart;
    }

    public String getLatitudeEnd() {
        return latitudeEnd;
    }

    public void setLatitudeEnd(String latitudeEnd) {
        this.latitudeEnd = latitudeEnd;
    }

    public String getLongitudeEnd() {
        return longitudeEnd;
    }

    public void setLongitudeEnd(String longitudeEnd) {
        this.longitudeEnd = longitudeEnd;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(int trip_id) {
        this.trip_id = trip_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
