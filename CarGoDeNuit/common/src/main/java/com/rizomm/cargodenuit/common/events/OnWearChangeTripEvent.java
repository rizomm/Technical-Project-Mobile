package com.rizomm.cargodenuit.common.events;

public class OnWearChangeTripEvent {
    private boolean mNext;

    public OnWearChangeTripEvent(boolean mNext) {
        this.mNext = mNext;
    }

    public boolean isNext() {
        return mNext;
    }

    public void setNext(boolean mNext) {
        this.mNext = mNext;
    }
}
