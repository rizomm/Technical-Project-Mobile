package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "path")
public class Trip {
    public static final String FIELD_PATH_NUMBER_PLACE     = "pathNumberPlace";
    public static final String FIELD_PATH_LATITUDE_START   = "pathLatitudeStart";
    public static final String FIELD_PATH_LONGITUDE_START  = "pathLongitudeStart";
    public static final String FIELD_PATH_LATITUDE_END     = "pathLatitudeEnd";
    public static final String FIELD_PATH_LONGITUDE_END    = "pathLongitudeEnd";
    public static final String FIELD_PATH_DATE_START       = "pathDateStart";
    public static final String FIELD_PATH_DATE_END         = "pathDateEnd";
    public static final String FIELD_PATH_USER_ID          = "pathUserId";

    @DatabaseField(columnName = FIELD_PATH_NUMBER_PLACE)
    private int nbPlace;

    @DatabaseField(columnName = FIELD_PATH_LATITUDE_START)
    private String latitudeStart;

    @DatabaseField(columnName = FIELD_PATH_LONGITUDE_START)
    private String longitudeStart;

    @DatabaseField(columnName = FIELD_PATH_LATITUDE_END)
    private String latitudeEnd;

    @DatabaseField(columnName = FIELD_PATH_LONGITUDE_END)
    private String longitudeEnd;

    @DatabaseField(columnName = FIELD_PATH_DATE_START)
    private String dateStart;

    @DatabaseField(columnName = FIELD_PATH_DATE_END)
    private String dateEnd;

    @DatabaseField(columnName = FIELD_PATH_USER_ID)
    private int userId;

    public Trip() {}

    public int getNbPlace() {
        return nbPlace;
    }

    public void setNbPlace(int nbPlace) {
        this.nbPlace = nbPlace;
    }

    public String getLatitudeStart() {
        return latitudeStart;
    }

    public void setLatitudeStart(String latitudeStart) {
        this.latitudeStart = latitudeStart;
    }

    public String getLongitudeStart() {
        return longitudeStart;
    }

    public void setLongitudeStart(String longitudeStart) {
        this.longitudeStart = longitudeStart;
    }

    public String getLatitudeEnd() {
        return latitudeEnd;
    }

    public void setLatitudeEnd(String latitudeEnd) {
        this.latitudeEnd = latitudeEnd;
    }

    public String getLongitudeEnd() {
        return longitudeEnd;
    }

    public void setLongitudeEnd(String longitudeEnd) {
        this.longitudeEnd = longitudeEnd;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
