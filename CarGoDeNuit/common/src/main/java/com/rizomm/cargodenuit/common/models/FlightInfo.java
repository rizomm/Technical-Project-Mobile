package com.rizomm.cargodenuit.common.models;

public class FlightInfo {

    // Fields
    // --------------------------------------------------------------------------------------------
    private String mAirportCode;
    private long mDegrees;
    private long mDistance;
    private long mTimeToArrive;
    private String mNextStep;
    private String mPreviousStep;

    // Constructor
    // --------------------------------------------------------------------------------------------
    public FlightInfo() {
    }

    // Getters & Setters
    // --------------------------------------------------------------------------------------------
    public String getAirportCode() {
        return mAirportCode;
    }

    public void setAirportCode(String mAirportCode) {
        this.mAirportCode = mAirportCode;
    }

    public long getDegrees() {
        return mDegrees;
    }

    public void setDegrees(long mDegrees) {
        this.mDegrees = mDegrees;
    }

    public long getDistance() {
        return mDistance;
    }

    public void setDistance(long mDistance) {
        this.mDistance = mDistance;
    }

    public long getTimeToArrive() {
        return mTimeToArrive;
    }

    public void setTimeToArrive(long mTimeToArrive) {
        this.mTimeToArrive = mTimeToArrive;
    }

    public String getPreviousStep() {
        return mPreviousStep;
    }

    public void setPreviousStep(String mPreviousStep) {
        this.mPreviousStep = mPreviousStep;
    }

    public String getNextStep() {
        return mNextStep;
    }

    public void setNextStep(String mNextStep) {
        this.mNextStep = mNextStep;
    }

    // Equals & Hashcode
    // --------------------------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FlightInfo that = (FlightInfo) o;

        if (mDegrees != that.mDegrees) return false;
        if (mDistance != that.mDistance) return false;
        if (mTimeToArrive != that.mTimeToArrive) return false;
        if (mAirportCode != null ? !mAirportCode.equals(that.mAirportCode) : that.mAirportCode != null)
            return false;
        if (mNextStep != null ? !mNextStep.equals(that.mNextStep) : that.mNextStep != null)
            return false;
        if (mPreviousStep != null ? !mPreviousStep.equals(that.mPreviousStep) : that.mPreviousStep != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = mAirportCode != null ? mAirportCode.hashCode() : 0;
        result = 31 * result + (int) (mDegrees ^ (mDegrees >>> 32));
        result = 31 * result + (int) (mDistance ^ (mDistance >>> 32));
        result = 31 * result + (int) (mTimeToArrive ^ (mTimeToArrive >>> 32));
        result = 31 * result + (mNextStep != null ? mNextStep.hashCode() : 0);
        result = 31 * result + (mPreviousStep != null ? mPreviousStep.hashCode() : 0);
        return result;
    }
}
