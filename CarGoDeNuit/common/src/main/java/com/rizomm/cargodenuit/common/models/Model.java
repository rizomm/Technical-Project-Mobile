package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "model")
public class Model {
    public static final String FIELD_MODEL_ID       = "modelId";
    public static final String FIELD_MODEL_NAME     = "modelName";
    public static final String FIELD_MODEL_BRAND_ID = "modelBrandId";

    @DatabaseField(columnName = FIELD_MODEL_ID)
    private int id;

    @DatabaseField(columnName = FIELD_MODEL_NAME)
    private String name;

    @DatabaseField(columnName = FIELD_MODEL_BRAND_ID)
    private int brand_id;

    public Model() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }
}
