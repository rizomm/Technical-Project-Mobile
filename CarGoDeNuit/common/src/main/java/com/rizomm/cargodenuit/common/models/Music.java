package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "music")
public class Music {
    public static final String FIELD_MUSIC_ID   = "musicId";
    public static final String FIELD_MUSIC_NAME = "musicName";

    @DatabaseField(columnName = FIELD_MUSIC_ID)
    private int id;

    @DatabaseField(columnName = FIELD_MUSIC_NAME)
    private String name;

    public Music() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
