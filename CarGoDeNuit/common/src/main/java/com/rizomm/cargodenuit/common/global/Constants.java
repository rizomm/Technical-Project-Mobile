package com.rizomm.cargodenuit.common.global;

public class Constants {

    // "Next step" & "Previous step" messages
    public static final String PREVIOUS_STEP_MESSAGE_PATH = "/previousstep";
    public static final String NEXT_STEP_MESSAGE_PATH = "/nextstep";

    // "Start app" message
    public static final String START_APP_MESSAGE_PATH = "/startCargoDeNuit";

}