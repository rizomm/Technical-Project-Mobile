package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "base")
public class Example {

    // Constants
    // --------------------------------------------------------------------------------------------
    public static final String COLUMN_TYPE = "column";

    // Fields
    // --------------------------------------------------------------------------------------------
    @DatabaseField(id = true)
    private String mCode;

    @DatabaseField(columnName = COLUMN_TYPE)
    private String type;

    @DatabaseField
    private String mLibelle;

    @DatabaseField
    private double mLatitude;

    @DatabaseField
    private double mLongitude;

    // Constructor
    // --------------------------------------------------------------------------------------------
    public Example() {}

    // Getters & Setters
    // --------------------------------------------------------------------------------------------
    public String getCode() {
        return mCode;
    }

    public void setCode(String mCode) {
        this.mCode = mCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLibelle() {
        return mLibelle;
    }

    public void setLibelle(String mLibelle) {
        this.mLibelle = mLibelle;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    // Equals & Hashcode
    // --------------------------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Example example = (Example) o;

        if (mCode != null ? !mCode.equals(example.mCode) : example.mCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return mCode != null ? mCode.hashCode() : 0;
    }
}
