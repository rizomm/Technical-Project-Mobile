package com.rizomm.cargodenuit.common.helpers;

import android.util.Log;

import com.rizomm.cargodenuit.common.global.Settings;

public class Logger {

    // Enum for logging depth
    // --------------------------------------------------------------------------------------------
    public enum LOGGER_DEPTH {
        ACTUAL_METHOD(4),
        LOGGER_METHOD(3),
        STACK_TRACE_METHOD(1),
        JVM_METHOD(0);

        private final int value;

        private LOGGER_DEPTH(final int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    // Constants
    // --------------------------------------------------------------------------------------------
    private static final String LOG_TAG = Logger.class.getSimpleName();

    // Automatic tac retrieval
    // --------------------------------------------------------------------------------------------
    private static String getTag(LOGGER_DEPTH depth) {
        StringBuilder sb = new StringBuilder();

        try {
            String className = Thread.currentThread().getStackTrace()[depth.getValue()].getClassName();
            sb.append(className.substring(className.lastIndexOf(".") + 1));
            sb.append("[");
            sb.append(Thread.currentThread().getStackTrace()[depth.getValue()].getMethodName());
            sb.append("] - ");
            sb.append(Thread.currentThread().getStackTrace()[depth.getValue()].getLineNumber());
            return sb.toString();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error when retrieving tag for custom logger", e);
        } finally {
            sb.setLength(0);
        }
        return null;
    }

    // Actual logging methods
    // --------------------------------------------------------------------------------------------

    // Verbose
    public static void Verbose(String msg) {
        if (Settings.isDebugEnvironment) {
            Log.v(getTag(LOGGER_DEPTH.ACTUAL_METHOD), msg);
        }
    }

    // Debug
    public static void Debug(String msg) {
        if (Settings.isDebugEnvironment) {
            Log.d(getTag(LOGGER_DEPTH.ACTUAL_METHOD), msg);
        }
    }

    // Info
    public static void Info(String msg) {
        if (Settings.isDebugEnvironment) {
            Log.i(getTag(LOGGER_DEPTH.ACTUAL_METHOD), msg);
        }
    }

    // Warning
    public static void Warning(String msg) {
        if (Settings.isDebugEnvironment) {
            Log.w(getTag(LOGGER_DEPTH.ACTUAL_METHOD), msg);
        }
    }

    public static void Warning(String msg, Throwable tr) {
        if (Settings.isDebugEnvironment) {
            Log.w(getTag(LOGGER_DEPTH.ACTUAL_METHOD), msg, tr);
        }
    }

    // Error
    public static void Error(String msg) {
        if (Settings.isDebugEnvironment) {
            Log.e(getTag(LOGGER_DEPTH.ACTUAL_METHOD), msg);
        }
    }

    public static void Error(String msg, Throwable tr) {
        if (Settings.isDebugEnvironment) {
            Log.e(getTag(LOGGER_DEPTH.ACTUAL_METHOD), msg, tr);
        }
    }

    // What a Terrible feature
    public static void Wtf(String msg) {
        if (Settings.isDebugEnvironment) {
            Log.wtf(getTag(LOGGER_DEPTH.ACTUAL_METHOD), msg);
        }
    }

    public static void Wtf(String msg, Throwable tr) {
        if (Settings.isDebugEnvironment) {
            Log.wtf(getTag(LOGGER_DEPTH.ACTUAL_METHOD), msg, tr);
        }
    }
}
