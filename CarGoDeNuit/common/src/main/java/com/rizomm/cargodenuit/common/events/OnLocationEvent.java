package com.rizomm.cargodenuit.common.events;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

public class OnLocationEvent implements Parcelable {

    // Parcelable Constructor
    // --------------------------------------------------------------------------------------------
    private Location location;

    public OnLocationEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return this.location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        this.location.writeToParcel(dest, flags);
    }

    // Parcelable Constructor
    // --------------------------------------------------------------------------------------------
    public static final Creator<OnLocationEvent> CREATOR = new Creator<OnLocationEvent>() {
        @Override
        public OnLocationEvent createFromParcel(Parcel source) {
            return new OnLocationEvent(source);
        }

        @Override
        public OnLocationEvent[] newArray(int size) {
            return new OnLocationEvent[size];
        }
    };

    public OnLocationEvent(Parcel in) {
        this.location = Location.CREATOR.createFromParcel(in);
    }
}

