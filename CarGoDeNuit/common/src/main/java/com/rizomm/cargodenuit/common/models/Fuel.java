package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "fuel")
public class Fuel {
    public static final String FIELD_FUEL_ID = "fuelId";
    public static final String FIELD_FUEL_NAME = "fuelName";

    @DatabaseField(columnName = FIELD_FUEL_ID)
    private int id;

    @DatabaseField(columnName = FIELD_FUEL_NAME)
    private String name;

    public Fuel() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
