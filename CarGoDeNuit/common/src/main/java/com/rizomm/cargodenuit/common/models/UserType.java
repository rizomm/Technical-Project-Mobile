package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "userType")
public class UserType {
    public static final String FIELD_USER_TYPE_ID   = "userTypeId";
    public static final String FIELD_USER_TYPE      = "userType";

    @DatabaseField(columnName = FIELD_USER_TYPE_ID)
    private int id;

    @DatabaseField(columnName = FIELD_USER_TYPE)
    private String type;

    public UserType() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
