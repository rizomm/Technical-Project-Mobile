package com.rizomm.cargodenuit.common.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "car")
public class Car {
    public static final String FIELD_CAR_ID                = "carId";
    public static final String FIELD_CAR_COLOR             = "carColor";
    public static final String FIELD_CAR_RELEASE           = "carRelease";
    public static final String FIELD_CAR_NUMBER_PLACE      = "carNumberPlace";
    public static final String FIELD_CAR_USB_KEY           = "carUsbKey";
    public static final String FIELD_CAR_CD                = "carCD";
    public static final String FIELD_CAR_NUMBER_GREY_CARD  = "carNumberGreyCard";
    public static final String FIELD_CAR_FUEL_ID           = "carFuelID";
    public static final String FIELD_CAR_MODEL_ID          = "carModelID";

    @DatabaseField(columnName = FIELD_CAR_ID)
    private int id;

    @DatabaseField(columnName = FIELD_CAR_COLOR)
    private String color;

    @DatabaseField(columnName = FIELD_CAR_RELEASE)
    private String issuanceDate;

    @DatabaseField(columnName = FIELD_CAR_NUMBER_PLACE)
    private int numberOfSeat;

    @DatabaseField(columnName = FIELD_CAR_USB_KEY)
    private boolean usb;

    @DatabaseField(columnName = FIELD_CAR_CD)
    private boolean cd;

    @DatabaseField(columnName = FIELD_CAR_NUMBER_GREY_CARD)
    private String grayCardNumber;

    @DatabaseField(columnName = FIELD_CAR_FUEL_ID)
    private int fuel_id;

    @DatabaseField(columnName = FIELD_CAR_MODEL_ID)
    private int model_id;

    public Car() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIssuanceDate() {
        return issuanceDate;
    }

    public void setIssuanceDate(String issuanceDate) {
        this.issuanceDate = issuanceDate;
    }

    public int getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(int numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    public boolean isUsb() {
        return usb;
    }

    public void setUsb(boolean usb) {
        this.usb = usb;
    }

    public boolean isCd() {
        return cd;
    }

    public void setCd(boolean cd) {
        this.cd = cd;
    }

    public String getGrayCardNumber() {
        return grayCardNumber;
    }

    public void setGrayCardNumber(String grayCardNumber) {
        this.grayCardNumber = grayCardNumber;
    }

    public int getFuel_id() {
        return fuel_id;
    }

    public void setFuel_id(int fuel_id) {
        this.fuel_id = fuel_id;
    }

    public int getModel_id() {
        return model_id;
    }

    public void setModel_id(int model_id) {
        this.model_id = model_id;
    }
}
